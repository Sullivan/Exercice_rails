class CreateScales < ActiveRecord::Migration[5.1]
  def change
    create_table :scales do |t|
      t.string :vehicle_type
      t.integer :coeff
      t.integer :horsepower

      t.timestamps
    end
  end
end
