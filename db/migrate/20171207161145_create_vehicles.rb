class CreateVehicles < ActiveRecord::Migration[5.1]
  def change
    create_table :vehicles do |t|
      t.references :user, foreign_key: true
      t.references :scale, foreign_key: true
      t.string :vehicle_name

      t.timestamps
    end
  end
end
