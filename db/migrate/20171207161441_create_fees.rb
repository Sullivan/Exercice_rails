class CreateFees < ActiveRecord::Migration[5.1]
  def change
    create_table :fees do |t|
      t.integer :km
      t.references :vehicle, foreign_key: true
      t.datetime :start_travel
      t.datetime :arrival_travel
      t.string :start_adress
      t.string :arrival_adress
      t.text :description

      t.timestamps
    end
  end
end
