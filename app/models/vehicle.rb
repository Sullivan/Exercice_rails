class Vehicle < ApplicationRecord
  belongs_to :user
  belongs_to :scale
  has_many :fees
end
