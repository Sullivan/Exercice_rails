json.extract! scale, :id, :vehicle_type, :coeff, :horsepower, :created_at, :updated_at
json.url scale_url(scale, format: :json)
