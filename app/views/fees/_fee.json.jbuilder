json.extract! fee, :id, :km, :vehicle_id, :start_travel, :arrival_travel, :start_adress, :arrival_adress, :description, :created_at, :updated_at
json.url fee_url(fee, format: :json)
